//
//  PhotosViewController.swift
//  Photorama
//
//  Created by Mark Bragg on 10/7/17.
//  Copyright © 2017 Mark Bragg. All rights reserved.
//

import UIKit

class PhotosViewController: UIViewController {
    @IBOutlet var imageView: UIImageView!
    var store: PhotoStore!
    var isInterestingPhoto = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        togglePhoto()
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(PhotosViewController.togglePhoto))
        view.addGestureRecognizer(tapRecognizer)
    }
    
    @objc func togglePhoto() {
        if !isInterestingPhoto {
            store.fetchInterestingPhotos {
                (photosResult) -> Void in
                switch photosResult {
                case let .success(photos):
                    print("successfully found \(photos.count) photos")
                    if let firstPhoto = photos.first {
                        self.updateImageView(for: firstPhoto)
                    }
                case let .failure(error):
                    print("Error fetching iteresting photos: \(error)")
                }
            }
        } else {
            store.fetchRecentPhotos {
                (photosResult) -> Void in
                switch photosResult {
                case let .success(photos):
                    print("successfully found \(photos.count) photos")
                    if let firstPhoto = photos.first {
                        self.updateImageView(for: firstPhoto)
                    }
                case let .failure(error):
                    print("Error fetching iteresting photos: \(error)")
                }
            }
        }
        isInterestingPhoto = !isInterestingPhoto
    }
    
    func updateImageView(for photo: Photo) {
        store.fetchImage(for: photo) {
            (imageResult) -> Void in
            
            switch imageResult {
            case let .success(image):
                self.imageView.image = image
            case let .failure(error):
                print("Error downloading image: \(error)")
            }
        }
    }
    
}
